﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Reflection;

namespace SwApiTestProject.Framework
{
    class HttpClientFactory
    {
        private static HttpClientFactory instance;
        private static readonly object mock = new();

        public static HttpClientFactory Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (mock)
                    {
                        if (instance == null)
                            instance = new HttpClientFactory();
                    }
                }
                return instance;
            }
        }

        public static string Url
        {
            get
            {
                string assemblyPath = new Uri(Assembly.GetExecutingAssembly().Location).AbsolutePath;
                Configuration cfg = ConfigurationManager.OpenExeConfiguration(assemblyPath);
                return cfg.AppSettings.Settings["Url"].Value.TrimEnd(new char[] { '/' });
            }
        }

        public HttpClient CreateHttpClient() => new()
        {
            BaseAddress = new Uri(Url)
        };
    }
}
