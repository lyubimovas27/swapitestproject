﻿using System;
using System.Net.Http;

namespace SwApiTestProject.Framework
{
    public class HttpClientContext
    {
        private HttpClient _client;

        public bool IsHttpClientSet => _client != null;
        public HttpClient HttpClient
        {
            get
            {
                if (_client == null)
                    throw new ApplicationException("Не установлен HttpClient, обычно это делается с помощью тэга Backend");
                return _client;
            }
            set { _client = value; }
        }
    }
}
