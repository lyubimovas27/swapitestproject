﻿using Newtonsoft.Json;
using System.Net.Http;

namespace SwApiTestProject.Framework
{
    public abstract class FeatureBase
    {
        HttpClientContext _clientContext;
        HttpResponseMessage responseBody;

        public FeatureBase(HttpClientContext httpClientContext)
        {
            _clientContext = httpClientContext;
        }

        public HttpClient CurrentClient => _clientContext.HttpClient;
        public string Message => responseBody.Content.ReadAsStringAsync().Result;
        public int StatusCode => (int)responseBody.StatusCode;

        public void GetRequest(string apiAddress) =>
            responseBody = CurrentClient.GetAsync(apiAddress).Result;

        public T GetResponseBody<T>() => JsonConvert.DeserializeObject<T>(Message);
    }
}
