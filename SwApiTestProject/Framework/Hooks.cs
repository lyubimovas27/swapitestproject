﻿using System;
using TechTalk.SpecFlow;

namespace SwApiTestProject.Framework
{
    [Binding]
    public sealed class Hooks
    {
        private readonly HttpClientContext _context;

        public Hooks(HttpClientContext context) => _context = context;

        [BeforeScenario("Backend")]
        public void EnsureHttpClient()
        {
            if (!_context.IsHttpClientSet)
            {
                InitializeHttpClient();
            }
        }

        private void InitializeHttpClient() => _context.HttpClient = HttpClientFactory.Instance.CreateHttpClient();

        [AfterScenario("Backend")]
        public void CleanUpHttpClient()
        {
            try
            {
                if (_context.IsHttpClientSet)
                {
                    _context.HttpClient.Dispose();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Error on CleanUpHttpClient: {exception}");
            }
        }
    }
}
