﻿@Backend
Feature: Тесты по swapi.dev/api/people

Scenario Outline: Получение информации по персонажам
	When я отправляю запрос на персонажа <Number>
	Then я получаю ответ по персонажу со статусом 200
	And я получаю информацию по персонажу "<Name>"

Examples: 
| Number | Name           |
| 1      | Luke Skywalker |
| 2      | C-3PO          |