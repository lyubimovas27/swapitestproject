﻿@Backend
Feature: Тесты по swapi.dev/api/starships

Scenario Outline: Получение информации по кораблю
	When я отправляю запрос на корабль <Number>
	Then я получаю ответ по кораблю со статусом 200
	And я получаю информацию по кораблю "<Name>"

Examples: 
| Number | Name              |
| 10     | Millennium Falcon |
| 3      | Star Destroyer    |