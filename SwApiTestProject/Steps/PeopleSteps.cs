﻿using NUnit.Framework;
using SwApiTestProject.Entities;
using SwApiTestProject.Framework;
using TechTalk.SpecFlow;

namespace SwApiTestProject.Steps
{
    [Binding]
    public sealed class PeopleSteps : FeatureBase
    {
        public PeopleSteps(HttpClientContext httpClientContext) : base(httpClientContext)
        {
        }

        [When(@"я отправляю запрос на персонажа (.*)")]
        public void WhenЯОтправляюЗапросНаПерсонажа(int number)
        {
            GetRequest($"/api/people/{number}");
        }

        [Then(@"я получаю ответ по персонажу со статусом (.*)")]
        public void ThenЯПолучаюОтветПоПерсонажуСоСтатусом(int code)
        {
            Assert.AreEqual(code, StatusCode);
        }

        [Then(@"я получаю информацию по персонажу ""(.*)""")]
        public void ThenЯПолучаюИнформациюПоПерсонажу(string name)
        {
            var people = GetResponseBody<People>();
            Assert.AreEqual(name, people.Name);
        }
    }
}
