﻿using NUnit.Framework;
using SwApiTestProject.Entities;
using SwApiTestProject.Framework;
using TechTalk.SpecFlow;

namespace SwApiTestProject.Steps
{
    [Binding]
    public sealed class StarshipsSteps : FeatureBase
    {
        public StarshipsSteps(HttpClientContext httpClientContext) : base(httpClientContext)
        {
        }

        [When(@"я отправляю запрос на корабль (.*)")]
        public void WhenЯОтправляюЗапросНаКорабльAsync(int number)
        {
            GetRequest($"/api/starships/{number}");
        }

        [Then(@"я получаю ответ по кораблю со статусом (.*)")]
        public void ThenЯПолучаюОтветПоКораблюСоСтатусом(int code)
        {
            Assert.AreEqual(code, StatusCode);
        }


        [Then(@"я получаю информацию по кораблю ""(.*)""")]
        public void ThenЯПолучаюИнформациюПоКораблю(string starShipName)
        {
            var starship = GetResponseBody<Starship>();
            Assert.AreEqual(starShipName, starship.Name);
        }

    }
}
